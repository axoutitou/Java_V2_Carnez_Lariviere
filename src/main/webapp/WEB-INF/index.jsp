<%-- 
    Document   : index
    Created on : 24 oct. 2019, 08:59:41
    Author     : Axel Carnez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <title>Java JEE- Projet Final</title>
  </head> 

  <body>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-4">
            
         <c:if  test="${!empty error}">
            <span style="color:red">  ${ error} </span><br/>
          </c:if>
            
          <div class="card">
            <div class="card-header">Login</div>
            <div class="card-body">

              <form action="Controleur" method="POST">
                <div class="form-group row">
                  <div class="col-md-12">
                    <input type="text" id="login" class="form-control" name="login" placeholder="Login" autofocus>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <input type="password" id="password" class="form-control" name="password" placeholder="Mot de passe">
                  </div>
                </div>

                <div class="col-md-2">
                  <button type="submit" class="btn btn-primary" value="Login" name="btnLogin">Login</button>  
                </div>
              </form>

            </div>
          </div>
        </div>  
      </div>
    </div>
  </body>

</html>
